var EffyTools = function() {

}

EffyTools.prototype.fittsLaw = function(distance, size){

    // calculate the time to position to a target
    // distance is the millimeters to the center of the target
    // size is the width along the axis of motion

    var a = 50; // constant for the start/stop time of the device
    var b = 150; // constant for the inherent slope of the device

    // returns time in milliseconds
    var time = a + b * (Math.log(distance / size + 1) / Math.log(2));

    return time;
}

EffyTools.prototype.hicksLaw = function(choices){

    // calculates the time to decide between several targets
    // choices is an integer of the number of targets

    var b = 1300; // constant of the time of one decision
    var time = b * (Math.log(choices + 1) / Math.log(2));

    // returns the time in milliseconds
    return time;
}


EffyTools.prototype.gomsConstants = {
    
    // measured in milliseconds

    K: 200, // the time it takes to tap on a keyboard key
    P: 1100, // pointing, the time it takes a user to point a position on the display
    H: 400, // homing, the time it takes a users hand to move from the keyboard to the graphical input device
    M: 1350, // mentally preparing, the time it takes to mentally prepare of the next step
    R: 200 // the time it takes the computer to respond
}

EffyTools.prototype.goms = function(actions){

    var goms = this.gomsConstants;
        
    // constants
    var times = {
        K: goms.K,           // keying
        P: goms.P,           // pointing to a target
        H: goms.H,           // changing input devices
        M: goms.M,           // mental preparation
        R: goms.R,           // time it takes computer to respond
        C: goms.P + goms.K,  // pointing with anticipated keying, will only include one M for both actions 
        k: goms.K,           // keying in a cognitive unit, will only include one M for the string
        E: goms.K,           // keying at the end of a cognitive unit, will not include M before
        A: goms.K,           // non variable keying, habitual keying, will not include M before
        p: goms.P            // a subpointing action, will not include an M before
    }

    // insert M in front of all 'K's
    actions = actions.replace(/K/g, 'MK');

    // insert M in front of all 'P's
    actions = actions.replace(/P/g, 'MP');

    // insert M in front of all 'P's
    actions = actions.replace(/C/g, 'MC');

    // insert M in from of series of 'k's
    actions = actions.replace(/(k+)/g, 'M$1');

    // delete any M adjacent to an R
    actions = actions.replace(/RM/g, 'R').replace(/MR/g, 'R');
    
    // calculate times
    var total = 0;
    var l = actions.length;
    for ( var i=0; i<l; i++ ){
        if ( !times[actions[i]] ) {
            console.log('\n', 'The mnemonic "'+ actions[i] + '" is not recognized.', '\n');
            process.exit();
        }
        total += times[actions[i]];
    }
    
    return total;
}

module.exports = EffyTools;
module.exports.tools = new EffyTools();