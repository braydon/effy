// phantomjs script

var page = require('webpage').create();
var tools = require('../../lib/tools').tools;
var async = require('async');
var system = require('system');

// add polyfill for bind (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }
        var aArgs = Array.prototype.slice.call(arguments, 1),
        fToBind = this,
        fNOP = function () {},
        fBound = function () {
            return fToBind.apply(this instanceof fNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
        };
        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();
        return fBound;
    };
}

page.viewportSize = {
    width: 1440,
    height: 900
};

var errors = [];

page.onError = function(msg, trace) {
    var error = new Error(msg);
    error.stack = trace;
    errors.push( error );
    return;
};

// set our initial cursor location
var cursor = {
    left: 700,
    top: 450
}

var screenDpi = 72; // pixels in the width of an inch
var screenDpm =  screenDpi / 25.4 // pixels in the width of a millimeter

navigationPath = false;
if ( system.args.length > 0) {
    try {
        navigationPath = JSON.parse(system.args[1]);
    } catch(ex) {
        console.log( new Error(ex) );
        phantom.exit();
    }
}

if ( !navigationPath || navigationPath.length <= 1 ) {
    console.log( new Error('Please specify more than one url to navigate') );
    phantom.exit();
}

var getHrefLocations = function(url, callback){
    var startTime = new Date();

    page.open(url, 'GET', function(status) {

        if (status !== 'success') {
            return callback(new Error('Failed to load url'));
        }

        var loadTime = new Date() - startTime;

        var elements = page.evaluate(function(s) {
            var links = document.getElementsByTagName('a');
            var l = links.length;
            var result = {};
            for(var i=0;i<l;i++){
                var href = links[i].href; // the full url
                if ( href && !result[href] ) { // only use the first known link
                    result[href] = {
                        left: links[i].offsetLeft,
                        top: links[i].offsetTop,
                        width: links[i].offsetWidth,
                        height: links[i].offsetHeight
                    }
                }
            }
            return result;
        });

        return callback(null, { elements: elements, loadTime : loadTime } );

    });

}

var calculateTimeToNavigate = function(startUrl, endUrl, callback){

    getHrefLocations( startUrl, function(err, result){

        if ( err ) return callback( err );

        if ( !endUrl ) {

            // return statistics
            return callback(null, {
                loadTime: result.loadTime,
                startUrl: startUrl,
                endUrl: endUrl
            });

        }

        var elements = result.elements;

        // calculate the time to decide between the number of elements
        var decisions = Object.keys(elements).length
        var decideTime = Math.round(tools.hicksLaw( decisions ));


        // find the href on the page
        var elm = elements[endUrl];
        if ( !elm ){
            return callback(new Error('The url `'+endUrl+'` could not be found at `'+startUrl+'`'));
        }

        // calculate the distance to the new target
        var diff = {
            left: cursor.left - elm.left,
            top: cursor.top - elm.top,
        }

        // calculate distance for cursor to move
        var distance = Math.sqrt(Math.pow(diff.left, 2) + Math.pow(diff.top, 2));

        // calculate the width of the target in the axis of motion (todo: review accuracy)
        if ( diff.top ) {
            var size = Math.abs(elm.height / diff.top) * distance;
        } else {
            var size = Math.abs(elm.width / diff.left) * distance;
        }

        // if the target is zero it would be impossible to click
        if ( size == 0 ) {
            return callback(new Error('The target `'+endUrl+'` had a width equal to zero.'));
        }

        // calculate time to move cursor using fitts law
        var navigateTime = Math.round(tools.fittsLaw(distance/screenDpm, size/screenDpm));

        // move cursor to the new position
        cursor.left = elm.left;
        cursor.top = elm.top;

        // return statistics
        callback(null, {
            cursorTravel: Math.round(distance),
            targetSize: Math.round(size),
            cursorPosition: {left: cursor.left, top: cursor.top},
            loadTime: result.loadTime,
            decisions: decisions,
            decideTime: decideTime,
            navigateTime: navigateTime,
            startUrl: startUrl,
            endUrl: endUrl
        });

    })
}

var Navigator = {
    startUrl: navigationPath[0], // add first item
    getTimes: function(endUrl, callback){
        var startUrl = this.startUrl;
        this.startUrl = endUrl;
        calculateTimeToNavigate( startUrl, endUrl, function(err, time){
            if ( err ) return callback(err);
            callback(null, time);
        })
    }
};

navigationPath = navigationPath.splice(1, navigationPath.length); // remove first item
navigationPath.push(false); // add empty navigation link at the end

async.mapSeries(navigationPath, Navigator.getTimes.bind(Navigator), function(err, results) {
    // output in json
    if ( err ) {
        errors.push(err);
        console.log(JSON.stringify({errors: errors}));
        phantom.exit();
    }
    console.log(JSON.stringify({data: results, errors: errors}));
    phantom.exit();
})
