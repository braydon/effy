#!/usr/bin/env node

var effy = require('commander');
var tools = require('../lib/tools').tools;
var async = require('async');
effy.version(require('../package').version);

effy.command('fitts')
    .description('calculate the time it takes to point to a target')
    .option('-d, --distance [distance]', 'the distance to the target in millimeters')
    .option('-w, --width [width]', 'the width of the target in millimeters in the axis of motion')
    .action(function(cmd){

        if (!cmd.distance || !cmd.width){
            console.log('\n', 'Please specify a --distance and --width argument ( effy fitts --help for more information )', '\n');
            process.exit();
        }

        var d = parseInt(cmd.distance);
        var w = parseInt(cmd.width);
        var time = Math.round(tools.fittsLaw( d, w ));
        console.log('\n', 'It takes Effy ' + time + ' milliseconds to point to a target ' + d + ' millimeters away with the width of ' + w + ' millimeters.', '\n')
        process.exit();
    });

effy.command('hicks')
    .description('calculate the time it takes to decide between targets')
    .option('-c, --choices [choices]', 'the number of targets')
    .action(function(cmd){

        if (!cmd.choices){
            console.log('\n', 'Please specify a --choices argument ( the number of targets )', '\n');
            process.exit();
        }

        var c = parseInt(cmd.choices);
        var time = Math.round(tools.hicksLaw( c ));
        console.log('\n', 'It takes Effy ' + time + ' milliseconds to choose between ' + c + ' targets.', '\n');
        process.exit();
    });

effy.command('goms')
    .description('calculate the time it takes to complete a task based on the goms model')
    .option('-a, --actions [actions]', 'the series of actions expressed in one-letter mnemonics')
    .on('--help', function(){
        console.log('  GOMS Mnemonics:');
        console.log('');
        console.log('    K = keying (a key press, click or touch)');
        console.log('    A = habitual-keying (a key press that is not variable and can become habitual)');
        console.log('    E = keying-end (a keying activity that denotes the end of a command)');
        console.log('    k = keying-series (a cognitive unit of keying, repeat for several key presses)');
        console.log('    C = point-and-key (a point followed by an anticipated keying )');
        console.log('    P = pointing (a pointing action)');
        console.log('    p = sub-pointing (a minor sub-pointing action, part of a larger pointing action)');
        console.log('    H = homing (changing input devices, such as keyboard to mouse)');
        console.log('');
    })
    .action(function(cmd){

        if (!cmd.actions){
            console.log('\n', 'Please specify --actions argument with GOMS Mneumonics ( effy goms --help for more information ) ', '\n');
            process.exit();
        }

        // calculate the total time
        var total = tools.goms( cmd.actions );

        // convert to seconds
        var seconds = total / 1000;

        console.log('\n', 'It takes Effy ' + seconds + ' seconds to complete this action.', '\n');
        process.exit();

    });

effy.command('navigate')
    .description('calculate the time it takes to navigate through a series of links')
    .option('-r, --route [route]', 'a json list of the paths to navigate')
    .on('--help', function(){
        console.log('');
        console.log('  Examples:');
        console.log('');
        console.log('    $ effy navigate --route \'["http://localhost:8080/index.html", "http://localhost:8080/one.html"]\'')
        console.log('');
    })
    .action(function(cmd){

        if (!cmd.route){
            console.log('\n', 'Please specify a --route param ( effy navigate --help for more information )', '\n');
            process.exit();
        }

        var path = require('path');
        var childProcess = require('child_process');
        var phantomjs = require('phantomjs');
        var binPath = phantomjs.path;

        var childArgs = [
            path.join(__dirname, '../scripts/phantomjs/navigate.js'),
            cmd.route
        ];

        childProcess.execFile(binPath, childArgs, function(err, stdout, stderr) {
            if ( err ) console.log(err);
            if ( stderr ) console.log(stderr);
            if ( stdout ) {
                var results = false;
                try {
                    results = JSON.parse( stdout );
                } catch (ex ) {
                    console.log('\n', 'Unexpected output from PhantomJS:\n\n' + stdout, '\n');
                }
                if ( results.errors ) {
                    for( var i=0;i<results.errors.length;i++){
                        console.log('\n', results.errors[i].message, '\n')
                        if ( results.errors[i].message ) {
                            console.log('\n', results.errors[i].stack, '\n')
                        }
                    }
                }
                if ( results.data ) {
                    async.reduce(results.data, 0, function(memo, item, callback){
                        var total = memo + item.loadTime;
                        if ( item.navigateTime ) total += item.navigateTime;
                        if ( item.decideTime ) total += item.decideTime;
                        callback( null, total );
                    }, function(err, time){
                        var seconds = time / 1000;
                        console.log('\n', 'It takes Effy ' + seconds + ' seconds to navigate in this route.', '\n');
                        console.log(' Statistics');
                        console.log(' ======================================================================', '\n');
                        for (var i=0;i<results.data.length;i++){
                            var task = results.data[i];
                            console.log(' Url:\t\t\t'+task.startUrl);
                            console.log(' Load Time:\t\t'+task.loadTime, 'milliseconds');
                            if ( task.decisions ) {
                                console.log(' Decisions:\t\t'+task.decisions, 'links');
                                console.log(' Decision Time:\t\t'+task.decideTime, 'milliseconds');
                                console.log(' Target:\t\t'+task.endUrl);
                                console.log(' Target Size:\t\t'+task.targetSize+' millimeters');
                                console.log(' Cursor Position:\t'+task.cursorPosition.top+' top', task.cursorPosition.left+' left');
                                console.log(' Cursor Travel:\t\t'+task.cursorTravel+' millimeters');

                                console.log(' Navigation Time:\t'+task.navigateTime, 'milliseconds \n\n');
                            }
                        }
                        console.log('')

                    })
                }
            }
            process.exit();
        })

    });

effy.command('*')
    .description('')
    .action(function(cmd){
        console.log('\n', 'Command is not recognized.');
        effy.help();
    })

effy.on('--help', function(){
    console.log('  Examples:');
    console.log('');
    console.log('    $ effy fitts -d 100 -w 20');
    console.log('    $ effy hicks -c 4');
    console.log('    $ effy goms -a HCHkkkkkE');
    console.log('    $ effy goms --help');
    console.log('    $ effy navigate --route \'["http://localhost:8080/index.html", "http://localhost:8080/one.html"]\'')
    console.log('');
});

effy.parse(process.argv);

if (!effy.args.length) return effy.help();