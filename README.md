Effy
==========================

Effy, a personified robot, helps determine the efficiency of computer interfaces using Fitt's and Hick's Laws and the GOMS Model.

Install using the command to include the effy command in your shell:

```
$ sudo npm install effy -g
```

To create an alias for development:

```
$ cd node_modules/effy && sudo npm link
```

